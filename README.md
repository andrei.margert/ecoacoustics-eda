# Ecoacoustics Exploratory Data Analysis

---
## !!! ABSOLUTELY CRITICAL SOFTWARE TO INSTALL !!!
Install `git-lfs` to you system to clone this repo with the actual results. 
Without `git-lfs` you WILL NOT CLONE THE RESULTS.

## Note for processing the data
***Remember to select and, if necessary, modify the appropriate configuration file prior to executing the processing.***

## Description
This repository houses a basic example of acoustic data processing utilizing the SciKit-MAAD library. 
The `notebooks/eda` directory features an example computation of acoustic properties from a dataset. 
The `conf/` directory houses a collection of YAML files, each correlating to a dataset, that delineate the parameters used for processing that particular dataset.

## Rename audio files
The MAAD library requires a specific file naming convention. 
To align your file names accordingly, use `rename_files.sh`. 
This bash script ensures your audio files are correctly parsed by the MAAD library.
To make this script executable, run the following command in the project root:
```commandline
chmod +x rename_files.sh
```

### Exmaple
Let's say you want to execute it on a list of files located at `/home/user_name/datasets/ecoacoustics/FS1`. Use this command:
```commandline
./rename_files.sh /home/user_name/datasets/ecoacoustics/FS1
```
You're encouraged to view the script implementation and adapt it as needed to meet your requirements.
