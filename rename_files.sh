#!/bin/bash

# define the directory to work with
dir_path=$1

# iterate over the .wav files in the directory
for old_file in "$dir_path"/*.wav
do
    # Extract the filename and extension
    filename=$(basename -- "$old_file")
    extension="${filename##*.}"
    filename="${filename%.*}"

    # Split the filename into parts using "_" as delimiter
    IFS="_"; read -a parts <<< "$filename"; unset IFS

    # Pad or truncate the XXX part to 8 characters
    parts[0]=$(printf "%-8.8s" "${parts[0]}")

    # Replace spaces with 'X'
    parts[0]="${parts[0]// /X}"

    # Construct new filename
    new_file="${parts[0]}_${parts[1]}_${parts[2]}.$extension"

    # Rename the file
    mv -- "$old_file" "$dir_path/$new_file"
    # Or print the new filenames without renaming
    # echo $new_file
done
